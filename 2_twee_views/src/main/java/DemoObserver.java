import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import be.kdg.model.AModel;
import be.kdg.model.BModel;
import be.kdg.view.APresenter;
import be.kdg.view.AView;
import be.kdg.view.BPresenter;
import be.kdg.view.BView;

/**
 * OPGELET: Sinds JDK 11 wordt JavaFX niet meer ondersteund.
 * Daarom kan je deze demo niet runnen op de gewone manier (rechtermuisklik 'Run' of via groen pijltje)
 * Je krijgt dan een foutmelding: "Error: JavaFX runtime components are missing, and are required to run this application"
 *
 * Je kan enkel runnen als volgt:
 * 1) Ofwel via Gradle venster (View > Tool Windows > Gradle) en daar de juiste module kiezen
 * en dan: Tasks > application > run
 * 2) Ofwel via Terminal venster onderaan en daar intikken: gradlew run
 */
public class DemoObserver extends Application {
    public void start(Stage firstStage) throws Exception {
        AModel aModel = new AModel();
        BModel bModel = new BModel();
        AView aView = new AView();
        BView bView = new BView();
        new APresenter(aModel, bModel, aView);
        new BPresenter(aModel, bModel, bView);

        Scene aScene = new Scene(aView);
        firstStage.setScene(aScene);
        firstStage.setTitle("AModel");
        firstStage.setX(300);
        firstStage.setY(200);
        firstStage.setWidth(260);
        firstStage.setHeight(150);
        firstStage.show();

        Stage secondStage = new Stage();
        Scene bScene = new Scene(bView);
        secondStage.setScene(bScene);
        secondStage.setTitle("BModel");
        secondStage.setX(600);
        secondStage.setY(200);
        secondStage.setWidth(260);
        secondStage.setHeight(150);
        secondStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
